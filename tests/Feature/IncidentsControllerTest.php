<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IncidentsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetAll()
    {

        $incidentsAll = \App\Incident::all();

        $this->call('GET', 'api/incidents')
        ->assertStatus(200)
        ->assertJsonCount(count($incidentsAll));
    }

    public function testFirstIncident()
    {
        $expectedObject = \App\Incident::find(1);

        $expected = json_encode($expectedObject);

        $this->call('GET', 'api/incidents/1')
            ->assertStatus(200);
    }

    public function testDestroyInexistantRecord()
    {
        $result = $this->call('DELETE', 'api/incidents/99999999')
            ->assertStatus(200);
    }

    public function testCreateUpdateDelete()
    {

        $mock = [
            'title' => 'unit testing',
            'description' => 'lorem ipsum dolor',
            'incident_level' => 3,
            'incident_type' => 2,
            'status' => 1,
        ];

        $response = $this->call('POST', 'api/incidents', $mock)
            ->assertStatus(201);

        $object = $response->original;

        $mock2 = [
            'title' => 'unit testing update',
            'description' => 'lorem ipsum dolor',
            'incident_level' => 2,
            'incident_type' => 3,
            'status' => 2,
        ];

        $this->call('PUT', 'api/incidents/'. $object->id, $mock2)
            ->assertStatus(200);

        $this->call('DELETE', 'api/incidents/'. $object->id)
            ->assertStatus(200);

    }


}
