<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IncidentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Incident::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
           'title' => 'required',
           'description' => 'required',
            'incident_level' => 'required|numeric|min:1|max:3',
            'incident_type' => 'required|numeric|min:1|max:4',
            'status' => 'required|numeric|min:1|max:2'
        ]);

        return \App\Incident::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \App\Incident::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'incident_level' => 'required|numeric|min:1|max:3',
            'incident_type' => 'required|numeric|min:1|max:4',
            'status' => 'required|numeric|min:1|max:2'
        ]);

        $incident = \App\Incident::find($id);
        $incident->title = $request->title;
        $incident->description = $request->description;
        $incident->incident_level = $request->incident_level;
        $incident->incident_type = $request->incident_type;
        $incident->status = $request->status;

        $incident->save();

        return $incident;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return \App\Incident::destroy($id);
    }
}
