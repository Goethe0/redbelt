﻿O teste foi dividido em partes.

## Parte 1, backend em Laravel 5.6.

Para o seu devido funcionamento, favor rodar o composer.

É necessário a configuração do banco de dados, no arquivo /.env e também a criação do schema redbelt, dentro do banco de dados configurado, para a criação das tabelas, favor rodar o migration, com o comando php artisan migration na raiz do projeto.

Como o escopo do projeto era limitado, foi criado models e o controller como resource, para agir de fato como uma api REST, caso fosse um projeto maior, iriamos criar e configurar todos os serviços de Middleware que o Laravel oferece.

## Parte 2, front-end em Angular 6.

Geralmente, costumo dividir em duas aplicações, uma de backend e outra de front-end, para maior facilidade de manutenção do código futuramente, porém, como se trata de um teste, integrei o Angular, dentro do Laravel, utilizando o sistema de rotas do mesmo.

Foram criadas, uma tela principal com um menu lateral responsivo.

Uma tela de listagem das incidências, no qual, se pode selecionar uma incidência para visualizar e/ou editar.

Criação de Incidências, seguindo as instruções dadas.

Todos os dados estão sendo salvos em banco e possuem validações simples no frontend e backend.

Como a estrutura de arquivos e diretórios do angular é grande, é fácil ficar um pouco perdido, mas os componentes, models, diretivas e serviços criados, estão nesse diretório: resources/assets/redbelt-frontend/src/app.


## Parte 3, Testes Unitários.

Foi feita uma camada de testes unitários, que possuí coverage de 100% de todos os métodos utilizados, pelo resource criado de Incidents, o report é salvo, na pasta /reports, e já está configurado para ser gerado automaticamente, ao rodar o phpunit.

Além do Gitlab, também deixei a aplicação rodando em um de meus servidores, a mesma pode ser acessada no seguinte endereço, detalhe, o servidor é compartilhado e em um plano bem em conta, então ele é bem lento.

http://goethedev.com

Dados de ftp, caso queiram validar o código:
ftp.goethedev.com
l: goethedev1
p: RedBelt!2

banco de dados:
redbelt.mysql.dbaas.com.br
schema: redbelt
l: redbelt 
p: redbelt

Normalmente, com mais tempos, criaria bancos de dados relacionais para o status, criticidade e tipo de incidente com a tabela Incidents, isso também aumentaria o número de validações que se pode fazer.

Acredito que seja isso, em relação ao design, dei menor importância, devido ao prazo curtíssimo, me concentrei em deixar o mesmo minimamente amigável, e compatível com mobile, o web app foi testado no Google Chrome.

Qualquer dúvida, me coloco a disposição.
Att.,
Diego.

# Cadastro de Incidentes.

Implementar em PHP e com um framework a sua escolha um CRUD para realizar o cadastro de incidentes. Cada inicidente possui:

- ID único
- Um título (obrigatório)
- Uma descrição  (obrigatório)
- Criticidade (Alta, média ou baixa)
- Um tipo (obrigatório)
- Um status (aberto ou fechado). Todo incidente deverá ser cadastrado com o status aberto

## O tipos possíveis para incidentes são:
- Ataque Brute Force
- Credencias vazadas
- Ataque de DDoS
- Atividades anormais de usuários

Implemente apenas as telas necessárias para fazer o CRUD, não há necessidade de implementar um login, por exemplo.

## O que será avaliado?

- Qualidade de código
- Validação dos dados inseridos pelo usuário
- Cobertura de testes.