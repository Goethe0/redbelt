import { Component } from '@angular/core';
import { IncidentService } from '../incident.service';
import { Incident } from '../incident';

@Component({
  selector: 'app-incident-new',
  templateUrl: './incident-new.component.html',
  styleUrls: ['./incident-new.component.css']
})
export class IncidentNewComponent {
  saved = false;
  incident: Incident = {
    title : '',
    description : '',
    incident_level : 2,
    incident_type : 1,
    status : 1
  };

  incidents: Array<Incident>;
  constructor(private incidentService: IncidentService) {

  }

  add() {
    const incident = Object.assign({}, this.incident);
    this.incidentService.createIncident(this.incident)
      .then((response) => {
        console.log(response);
        this.saved = true;
      })
  };
}
