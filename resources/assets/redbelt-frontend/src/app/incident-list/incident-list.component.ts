import { Component } from '@angular/core';
import { Incident } from '../incident';
import { IncidentService } from '../incident.service';

@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.css']
})
export class IncidentListComponent {
  incidents: Array<Incident>;

  constructor(private incidentService: IncidentService) {

    this.incidentService.getIncidents()
      .then((incidents:Array<Incident>) => this.incidents = incidents);
  }
}
