import { Component } from '@angular/core';
import { Incident } from './incident';
import { IncidentService } from './incident.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/sidebar.css', './app.component.css']
})
export class AppComponent {
  title = 'RedBelt - Incidentes - Diego S. Araujo';
}
