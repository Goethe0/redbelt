import { Injectable } from '@angular/core';
import { Incident } from './incident';
import { Http } from "@angular/http";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {toPromise} from "rxjs-compat/operator/toPromise";

@Injectable({
  providedIn: 'root'
})
export class IncidentService {

  baseUrl: string = '/api/incidents';
  incidents:Array<Incident> = [];

  constructor(private http: Http) { }

  getIncidents():Promise<Array<Incident>> {
    return this.http.get(this.baseUrl)
      .toPromise()
      .then(response => response.json());
  };

  getIncident(id) {
    return this.http.get(this.baseUrl + '/' + id)
      .map(res => res.json())

  }

  createIncident(incident:Incident) {
    return this.http.post(this.baseUrl, incident)
      .toPromise()
      .then(response => response.json());
  }

  updateIncident(incident:Incident, id) {
    return this.http.put(this.baseUrl + "/" + id, incident)
      .toPromise()
      .then(response => response.json());
  }

}
