import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { IncidentListComponent } from './incident-list/incident-list.component';
import { IncidentNewComponent } from './incident-new/incident-new.component';
import { IncidentService } from './incident.service';
import { IncidentEditComponent } from './incident-edit/incident-edit.component';

const appRoutes: Routes = [
  {  path: 'incidents', component: IncidentListComponent },
  {  path: 'incidents/new', component: IncidentNewComponent },
  {  path: 'incidents/edit/:id', component: IncidentEditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    IncidentListComponent,
    IncidentNewComponent,
    IncidentEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule
  ],
  providers: [
    IncidentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
