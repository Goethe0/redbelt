import { Component } from '@angular/core';
import {Incident} from "../incident";
import {IncidentService} from "../incident.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-incident-edit',
  templateUrl: './incident-edit.component.html',
  styleUrls: ['./incident-edit.component.css']
})
export class IncidentEditComponent {
  idIncident;
  saved = false;
  incident: Incident = {
    title : '',
    description : '',
    incident_level : 2,
    incident_type : 1,
    status : 1
  };

  constructor(private incidentService: IncidentService, private route: ActivatedRoute) {

    let id = this.route.params.subscribe(params => {
      let id = params['id'];
      this.idIncident = id;
      console.log(id);
      if (!id)
        return;

      this.incidentService.getIncident(id)
        .subscribe(incident => this.incident = incident);
    });


  }

  edit() {
    const incident = Object.assign({}, this.incident);
    this.incidentService.updateIncident(incident, this.idIncident)
      .then((response) => {
        console.log(response);
        this.saved = true;
      })
  };

}
