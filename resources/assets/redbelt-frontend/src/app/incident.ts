export class Incident {
  title: string;
  description: string;
  incident_level: number;
  incident_type: number;
  status: number;
}
